# The PyPro app

The vision of the pypro app is to provide in-depth information about 
companies all over the world.
It will help make people self-aware of the current economic and 
social status of companies and geographies. 
It will help people make fact-based investment decisions and 
provide all information they need to get to know about the companies
they are investing or are planning to invest.
It will provide smart-analysis: Categorizing data into 
meaningful categories to help provide transparency on each 
financial metric

## How to run the app
### Pre-requisites
Before running the app, make sure you have the following installed
[python 3.9.x](https://www.python.org/downloads/release/python-392/)

### Steps to run the app

  Open the terminal and point it to the project root directory and
  run the following commands one by one

1. Create a virtual environment

```
python3.9 -m venv venv
```

2. Activate the virtual environment
```
source venv/bin/activate
```

3. Install the project dependencies
```
pip3 install -r requirements.txt
```
4. Test installation
To ensure that the installation was successfull, run the command:
   ```
   make runserver
   ```
