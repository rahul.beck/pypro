from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
import json
import os


def get_content(body):
    return json.loads(body.decode('utf-8'))


def is_user_valid(creds: dict):
    user = authenticate(username=creds["username"], password=creds["pwd"])
    print(user)
    if user is not None:
        os.environ['IS_AUTHENTICATED'] = 'True'
        return "True"
    return "False"


@csrf_exempt
def auth(request):
    content = get_content(request.body)
    if request.method == 'POST':
        return HttpResponse(str(is_user_valid(content)))
    return HttpResponse("method not supported")
