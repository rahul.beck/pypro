from django.db import models


class Companies(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=50)
    primary_exchange = models.CharField(max_length=50)
    ticker = models.CharField(max_length=12)
    max_price = models.FloatField(default=0.0)
    min_price = models.FloatField(default=0.0)
