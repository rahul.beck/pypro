from django.urls import path
from . import views

urlpatterns = [
    path('<int:company_id>/', views.get_companies, name='get_companies')
]