from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import os


@csrf_exempt
def get_companies(request, company_id):
    if os.environ.get('IS_AUTHENTICATED') == 'True':
        if request.method == 'GET':
            return HttpResponse("Company id is {0}".format(str(company_id)))
        elif request.method == 'POST':
            return HttpResponse(request.body)
    return HttpResponse("User not authenticated")
